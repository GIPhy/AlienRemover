/*
  ########################################################################################################

  AlienRemover: a tool to quickly remove contaminant reads from FASTQ file(s)
    
  Copyright (C) 2021  Institut Pasteur
  
  This program  is free software:  you can  redistribute it  and/or modify it  under the terms  of the GNU
  General Public License as published by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,  but WITHOUT ANY WARRANTY;  without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
  License for more details.
  
  You should have received a copy of the  GNU General Public License along with this program.  If not, see
  <http://www.gnu.org/licenses/>.
  
  Contact:
   Alexis Criscuolo                                                            alexis.criscuolo@pasteur.fr
   Genome Informatics & Phylogenetics (GIPhy)                                             giphy.pasteur.fr
   Bioinformatics and Biostatistics Hub                                 research.pasteur.fr/team/hub-giphy
   USR 3756 IP CNRS                          research.pasteur.fr/team/bioinformatics-and-biostatistics-hub
   Dpt. Biologie Computationnelle                     research.pasteur.fr/department/computational-biology
   Institut Pasteur, Paris, FRANCE                                                     research.pasteur.fr

  ########################################################################################################
*/

//### NOTE ON INPUT FILE(S) ##############################################################################
//#   it is required that FASTA/FASTQ input files should be UTF-8-encoded
//### NOTE ON FASTQ FILE(S) ##############################################################################
//#   it is expected that no blank space occurs in line 2 and 4 of FASTQ blocks (i.e. sequenced read and associated Phred scores, respectively)
//#   it is also expected that no empty line occurs in the FASTQ files
//#   when paired, it is trivially expected that the two input files contain the same no. FASTQ blocks
//########################################################################################################

import java.io.*;
import java.nio.file.*;
import java.util.*;
import java.util.zip.*;

public class AlienRemover {

    //### constants  ################################################################
    final static   String VERSION           = "1.0.201209ac   Copyright (C) 2021  Institut Pasteur";
    static final   String NOTHING           = "N.o./.T.h.I.n.G";
    static final      int LARGE_BUFFER_SIZE = 1<<16;
    static final      int SMALL_BUFFER_SIZE = 1<<8; 
    static final   byte[] buffer            = new byte[SMALL_BUFFER_SIZE];
    static final     byte UTF8_NEWLINE      = (byte) 10;
    static final     byte UTF8_CARRIAGE     = (byte) 13;
    static final     byte UTF8_BLANK        = (byte) 32;
    static final     byte UTF8_CHEVRON      = (byte) 62;
    static final     byte UTF8_A            = (byte) 65;
    static final     byte UTF8_C            = (byte) 67;
    static final     byte UTF8_G            = (byte) 71;
    static final     byte UTF8_N            = (byte) 78;
    static final     byte UTF8_T            = (byte) 84;
    static final     byte UTF8_a            = (byte) 97;
    static final     byte UTF8_c            = (byte) 99;
    static final     byte UTF8_g            = (byte) 103;
    static final     byte UTF8_t            = (byte) 116;
    static final     byte BSIZE             = (byte) 2;
    static final     byte BA                = (byte) 0;
    static final     byte BC                = (byte) 1;
    static final     byte BG                = (byte) 2;
    static final     byte BT                = (byte) 3;
    static final     byte B0                = (byte) 0;
    static final     byte B1                = (byte) 1;
    static final   String DEFAULT_BASENAME  = "out";
    //                                  succ   0       1       2       3       4       5     
    //                                   sum   0.0000  0.0955  0.4410  1.0954  2.0000  3.0000
    static final double[] WGT               = {0.0000, 0.0955, 0.3455, 0.6545, 0.9045, 1.0000};  // (sin(PI*x/10))^2  =>  mu -= 2
    static final      int WGT_LGT           = 5;
    static final      int SUCC_MISMATCH     = 4;


    //### options   #################################################################
    static    File alienFile; // -a    alien sequence(s) (mandatory)
    static    File modelFile; // -b    model sequence(s)
    static    File infq1;     // -i/-1 (r1) fastq input file
    static    File infq2;     // -2    (r2) fastq input file
    static  String basename;  // -o    basename for output file(s) (default: out)
    static    byte k;         // -k    k-mer length (default: 31)
    static    long nk;        // -n    expected no. canonical k-mers (default: estimated from the file size)
    static  double fp;        // -p    false positive cutoff (default: 0.03)
    static boolean lbmh;      // -l    less bits, more hashing functions (default: not set)
    static  double cutoff;    // -c    cutoff to discard a read (default: 0.2)
    static boolean wrm;       // -r    to write discarded FASTQ blocks (default: not set)
    static boolean wkmr;      // -w    to write k-mer file (default: not set) 
    static boolean gz;        // -z    gzipped output files
     
    //### io   ######################################################################
    static              String filename;
    static BufferedInputStream bis;
    static   ObjectInputStream ois;
    static      BufferedReader in1, in2;
    static      BufferedWriter out1, out2, rmout1, rmout2;
    
    //### data   ####################################################################
    static    long t;          // starting date in milliseconds
    static boolean paired;     // true when paired fastq files
    static    long msk;        // mask := BSIZE^k - 1
    static    long del, drc;   // 
    static    byte k_1;        // k-1
    static    long rcA;        // coding of reverse-complement A
    static    long rcC;        // coding of reverse-complement C
    static    long rcG;        // coding of reverse-complement G
    static    long rcT;        // coding of reverse-complement T
    static    LHBF bloom;      // Less-Hash Bloom Filter
    static boolean model;      // true when mloob is not empty
    static    LHBF mloob;      // model Bloom filter
    static  String l1_1, l2_1; // line 1 of FASTQ block
    static  String l1_2, l2_2; // line 2 of FASTQ block
    static  String l1_3, l2_3; // line 3 of FASTQ block
    static  String l1_4, l2_4; // line 4 of FASTQ block
    static boolean discard;    // true when read entry to discard
    static    long cnt_r_in1;  // no. reads in R1 input file
    static    long cnt_r_in2;  // no. reads in R2 input file
    static    long cnt_b_in1;  // no. bases in R1 input file
    static    long cnt_b_in2;  // no. bases in R2 input file
    static    long cnt_r_out1; // no. reads in R1 output file
    static    long cnt_r_out2; // no. reads in R2 output file
    static    long cnt_b_out1; // no. bases in R1 output file
    static    long cnt_b_out2; // no. bases in R2 output file

    //### stuffs   ##################################################################
    static    byte skip;
    static     int b, i, j, o, x, y;
    static     int h1, h2, h, lgt, succ, nc, mu;
    static boolean r, clear;
    static    long kmr, krc, l, c, xy;
    static  double d, p, crit, limit, p_1;
    static  byte[] ba;
    
    public static void main(String[] args) throws IOException {

	//#############################################################################################################
	//#############################################################################################################
	//### doc                                                                                                   ###
	//#############################################################################################################
	//#############################################################################################################
	System.out.println("");
	System.out.println("AlienRemover v" + VERSION);
	System.out.println("");
	if ( args.length < 2 ) {
	    System.out.println(" Fast removal of alien reads (contaminant, host, ...) from FASTQ file(s)");
	    System.out.println("");
	    System.out.println(" USAGE:");
	    System.out.println("   AlienRemover -a <alienfile> [-b <modelfile>]       [-o <basename>] [-k <int>]");
	    System.out.println("   AlienRemover -a <alienfile>  -i <FASTQ>            [-o <basename>] [-k <int>] [-c <float>] [-p <float>] [...]");
	    System.out.println("   AlienRemover -a <alienfile>  -1 <FASTQ> -2 <FASTQ> [-o <basename>] [-k <int>] [-c <float>] [-p <float>] [...]");
	    System.out.println("");
	    System.out.println(" OPTIONS:");
	    System.out.println("    -a <infile>   FASTA file containing alien sequence(s); filename should end with .gz when gzipped");
	    System.out.println("    -a <infile>   input file  containing alien  k-mers generated  by AlienRemover  from  FASTA-formatted  alien");
	    System.out.println("                  sequence(s); filename should end with .kmr or .kmz");
	    System.out.println("    -i <infile>   [SE] FASTQ-formatted input file; filename should end with .gz when gzipped");
	    System.out.println("    -1 <infile>   [PE] FASTQ-formatted R1 input file; filename should end with .gz when gzipped");
	    System.out.println("    -2 <infile>   [PE] FASTQ-formatted R2 input file; filename should end with .gz when gzipped");
	    System.out.println("    -o <name>     outfile basename; output files have the following extensions:");
	    System.out.println("                   + alien k-mers: <name>.km<r|z>");
	    System.out.println("                   + SE reads:     <name>.fastq[.gz]                        (.gz is added when using option -z)");
	    System.out.println("                   + PE reads:     <name>.1.fastq[.gz] <name>.2.fastq[.gz]  (.gz is added when using option -z)");
	    System.out.println("    -k [10-31]    k-mer length for alien sequence occurence searching; must lie between 10 and 31 (default: 25)");
	    System.out.println("    -p <float>    Bloom filter false positive probability cutoff (default: 0.05)");
	    System.out.println("    -n <integer>  expected number of canonical k-mers (default: estimated from the alien file size)");
	    System.out.println("    -l            use less bits and more hashing functions, whenever possible (default: not set)");
	    System.out.println("    -c <float>    criterion to remove a read (default: 0.15)");
	    System.out.println("    -s            compute Bloom filter statistics (default: not set)");
	    System.out.println("    -w            write Bloom filter into output file (default: not set)");
	    System.out.println("    -r            write removed reads into output file(s) (default: not set)");
	    System.out.println("    -z            gzipped output files (default: not set)");
	    System.out.println("");
	    System.out.println(" EXAMPLES:");
	    System.out.println("   AlienRemover  -a alien.fasta                   -o alien      -k 30     ");
	    System.out.println("   AlienRemover  -a alien.kmr   -i reads.fastq    -o flt_reads  --p64   -z");
	    System.out.println("   AlienRemover  -a alien.kmr   -1 r1.fq -2 r2.fq               -c 0.3  -r");
	    System.out.println("");
	    System.exit(1);
	}

	
	//#############################################################################################################
	//#############################################################################################################
	//### reading options                                                                                       ###
	//#############################################################################################################
	//#############################################################################################################
	alienFile = new File(NOTHING);  // -a
	modelFile = new File(NOTHING);  // -b
	model     = false;
	infq1     = new File(NOTHING);  // -1 / -i
	infq2     = new File(NOTHING);  // -2
	paired    = false;
	basename  = DEFAULT_BASENAME;   // -o
	k         = (byte) 25;          // -k
	nk        = 0;                  // -n
	fp        = 0.05;               // -p
	lbmh      = false;              // -l
	cutoff    = 0.15;               // -c
	wrm       = false;              // -r
	wkmr      = false;              // -w
	gz        = false;              // -z
	o = -1;
	while ( ++o < args.length ) {
	    if ( args[o].equals("-a") )     { alienFile = new File(args[++o]);                continue; }
	    if ( args[o].equals("-b") )     { modelFile = new File(args[++o]); model = true;  continue; }
	    if ( args[o].equals("-i") )     {     infq1 = new File(args[++o]);                continue; }
	    if ( args[o].equals("-1") )     {     infq1 = new File(args[++o]);                continue; }
	    if ( args[o].equals("-2") )     {     infq2 = new File(args[++o]); paired = true; continue; }
	    if ( args[o].equals("-o") )     {  basename = args[++o];                          continue; }
	    if ( args[o].equals("-k") ) try {         k = Byte.parseByte(args[++o]);          continue; } catch ( NumberFormatException e ) { System.err.println("incorrect value: " + args[o] + " (option -k)"); System.exit(1); }
	    if ( args[o].equals("-p") ) try {        fp = Double.parseDouble(args[++o]);      continue; } catch ( NumberFormatException e ) { System.err.println("incorrect value: " + args[o] + " (option -p)"); System.exit(1); }
	    if ( args[o].equals("-n") ) try {        nk = Long.parseLong(args[++o]);          continue; } catch ( NumberFormatException e ) { System.err.println("incorrect value: " + args[o] + " (option -n)"); System.exit(1); }
	    if ( args[o].equals("-l") )     {      lbmh = true;                               continue; }
	    if ( args[o].equals("-c") ) try {    cutoff = Double.parseDouble(args[++o]);      continue; } catch ( NumberFormatException e ) { System.err.println("incorrect value: " + args[o] + " (option -c)"); System.exit(1); }
	    if ( args[o].equals("-z") )     {        gz = true;                               continue; }
	    if ( args[o].equals("-w") )     {      wkmr = true;                               continue; }
	    if ( args[o].equals("-r") )     {       wrm = true;                               continue; }
	}	
	filename = alienFile.toString();
	if ( filename.equals(NOTHING)   )                         { System.err.println("no alien file (option -a)");                                             System.exit(1); }
	if ( ! alienFile.exists() )                               { System.err.println("file " + filename + " does not exist (option -a)");                      System.exit(1); }
	if ( model ) {
	    if ( ! modelFile.exists() )                           { System.err.println("file " + filename + " does not exist (option -b)");                      System.exit(1); }
	    filename = modelFile.toString();
	    if ( ! (filename.endsWith(".kmr")
		    || filename.endsWith(".kmz")) )               { System.err.println("model file should ends with .kmr or .kmz (option -b)");                  System.exit(1); }
	}
	filename = infq1.toString();
	if ( ! filename.equals(NOTHING) && ! infq1.exists() )     { System.err.println("file " + filename + " does not exist (option -1)");                      System.exit(1); }
	if ( paired ) {
	    filename = infq2.toString();
	    if ( ! filename.equals(NOTHING) && ! infq2.exists() ) { System.err.println("file " + filename + " does not exist (option -2)");                      System.exit(1); }
	}
	if ( k < 10 || k > 31 )                                   { System.err.println("k-mer length should be set between 10 and 31 (option -k)");              System.exit(1); }
	if ( fp < 0 || fp > 1 )                                   { System.err.println("FP probability cutoff should be set between 0 and 1 (option -p)");       System.exit(1); }
	if ( nk < 0 )                                             { System.err.println("expected number of canonical k-mers should be positive (option -n)");    System.exit(1); }
	if ( cutoff < 0 || 1 < cutoff )                           { System.err.println("the cutoff should be set between 0 and 1 (option -c)");                  System.exit(1); }

	
	//#############################################################################################################
	//#############################################################################################################
	//### init variables                                                                                        ###
	//#############################################################################################################
	//#############################################################################################################
	t = System.currentTimeMillis();
	filename = alienFile.toString();
	if ( filename.endsWith(".kmr") )
	    try { bloom = new LHBF(filename, false); k = bloom.k(); }                                                        catch ( ClassNotFoundException e ) {}
	    catch ( OutOfMemoryError e ) { System.err.println("not enough RAM: " + (alienFile.length()>>20) + "Mb required (java option -Xmx)"); System.exit(1); }
	if ( filename.endsWith(".kmz") )
	    try { bloom = new LHBF(filename, true); k = bloom.k(); }                                                         catch ( ClassNotFoundException e ) {}
	    catch ( OutOfMemoryError e ) { System.err.println("not enough RAM: " + (alienFile.length()>>19) + "Mb required (java option -Xmx)"); System.exit(1); }
	k_1 = k; --k_1;
	msk = 1L; msk <<= BSIZE * k; --msk;  //## NOTE: ooo1111111111111111 == 2*k 1s
	del = msk & (msk << BSIZE);          //## NOTE: ooo1111111111111100 == msk-3
	drc = msk >>> BSIZE;                 //## NOTE: ooo0011111111111111 == 2*(k-1) 1s
	rcA = BT; rcA <<= BSIZE * k_1;       //## NOTE: ooo1100000000000000
	rcC = BG; rcC <<= BSIZE * k_1;       //## NOTE: ooo1000000000000000
	rcG = BC; rcG <<= BSIZE * k_1;       //## NOTE: ooo0100000000000000
	rcT = BA; rcT <<= BSIZE * k_1;       //## NOTE: rcT == 0, because BA == 0
	//## counters
	cnt_r_in1  = cnt_r_in2  = cnt_b_in1  = cnt_b_in2  = 0;
	cnt_r_out1 = cnt_r_out2 = cnt_b_out1 = cnt_b_out2 = 0;


	//#############################################################################################################
	//#############################################################################################################
	//### reading alien file (option -a)                                                                        ###
	//#############################################################################################################
	//#############################################################################################################
	filename = alienFile.toString();
	if ( filename.endsWith(".kmr") || filename.endsWith(".kmz") ) {
	    System.out.println("input alien file (k=" + k + "):  " + filename +
			       " [b=" + bloom.l2nBits() + ";h=" + bloom.nHash() + ";n=" + String.format(Locale.US, "%,d", bloom.nElements()) + ";fp<" + String.format(Locale.US, "%.4f", bloom.fp()) + "]");
	}
	else {
	    //### creating Bloom filter  #####################################################################
	    if ( nk == 0L ) {
		nk = 9L * alienFile.length() / 10L;
		if ( filename.endsWith(".gz") ) nk = (nk * 7) / 2;
	    }
	    try {
		bloom = new LHBF(nk, fp, k, lbmh);
	    }
	    catch ( OutOfMemoryError e ) {
		System.err.println("[n=" + nk + "] " + Arrays.toString(LHBF.computeDimension(nk, fp, lbmh)).replaceAll("\\[", "[b=").replaceAll(", ", "] [h="));
		System.err.println("not enough RAM: " + LHBF.computeSizeMb(LHBF.computeDimension(nk, fp, lbmh)[0])  + "Mb required (java option -Xmx)");
		if ( ! lbmh ) System.err.println("you can also try with less bits and more hashing functions (option -l)");
		else          System.err.println("you can also try with little higher false positive probability cutoff (option -p)");
		System.exit(1);
	    }
	    //### k-mer decomposition of alien sequence(s) ###################################################
	    System.out.println("input alien file:         " + filename + " [" + (alienFile.length()>>20) + "Mb" + String.format(Locale.US, ";fp<%.6f]", LHBF.computeFP(bloom.l2nBits(), bloom.nHash(), nk)));
	    bis = ( filename.endsWith(".gz") )
		? new BufferedInputStream(new GZIPInputStream(Files.newInputStream(Path.of(filename)), LARGE_BUFFER_SIZE), LARGE_BUFFER_SIZE)
		: new BufferedInputStream(                    Files.newInputStream(Path.of(filename)                    ), LARGE_BUFFER_SIZE);
	    System.out.println("k-mer length:             " + k);
	    System.out.println("no. hashing:              " + bloom.nHash());
	    System.out.println("no. bits:                 " + String.format(Locale.US, "%,d", bloom.nBits()) + " (2^" + bloom.l2nBits() + ";" + LHBF.computeSizeMb(bloom.l2nBits()) + "Mb)");
	    r = false; 
	    c = 0;  //## NOTE: c = estimated no. distinct k-mers (only with v1 or v2)
	    while( bis.read(buffer) > 0 ) 
		for (final byte b: buffer) {
		    if ( ! r ) { if ( b == UTF8_NEWLINE ) { kmr = krc = B0; skip = k; r = true; } continue; }
		    switch ( b ) {
		    case UTF8_NEWLINE: case UTF8_CARRIAGE: case UTF8_BLANK:                       continue;
		    case UTF8_CHEVRON:                                                r = false;  continue;
		    }
		    kmr <<= BSIZE; krc >>>= BSIZE; 
		    switch ( b ) {
		    case UTF8_A: case UTF8_a:/*kmr |= BA;*/ krc |= rcA;   break;
		    case UTF8_C: case UTF8_c:  kmr |= BC;   krc |= rcC;   break;
		    case UTF8_G: case UTF8_g:  kmr |= BG;   krc |= rcG;   break;
		    case UTF8_T: case UTF8_t:  kmr |= BT; /*krc |= rcT;*/ break;
		    default:                   skip = k_1;             continue;
		    }
		    //# v1 ####
		    //if ( skip > 0 ) { --skip; continue; }
		    //if ( ! bloom.get(kmr & msk, krc) ) { ++c; bloom.set(kmr & msk, krc); }
		    //# v2 ####
		    //switch ( skip ) {
		    //case B0:            c = ( bloom.getset(kmr & msk, krc) ) ? c : ++c; continue;
		    //case B1: skip = B0; c = ( bloom.getset(kmr & msk, krc) ) ? c : ++c; continue;
		    //default:    --skip;                                                 continue;
		    //}
		    //# v3 ####
		    switch ( skip ) {
		    case B0:              bloom.set(kmr & msk, krc); continue;
		    case B1:   skip = B0; bloom.set(kmr & msk, krc); continue;
		    default: --skip;                                 continue;
		    }
		}
	    //### displaying stats ###########################################################################
	    System.out.println("no. set bits:             " + String.format(Locale.US, "%,d", bloom.cardinality()));
	    System.out.println("estimated no. k-mers:     " + String.format(Locale.US, "%,d", bloom.nElements())); //System.out.println("estimated no. k-mers:     " + String.format(Locale.US, "%,d", c));
	    System.out.println("false positive proba.:    " + String.format(Locale.US, "%,6f", bloom.fp()));
	    bis.close();
	    //### writing bloom filter(s) ####################################################################
	    if ( wkmr ) {
		filename = basename + (( gz ) ? ".kmz" : ".kmr");
		System.out.println("output k-mer file:        " + filename);
		bloom.save(filename, gz);
	    }
	}

	
	//#############################################################################################################
	//#############################################################################################################
	//### reading model file (option -b)                                                                        ###
	//#############################################################################################################
	//#############################################################################################################
	mloob = new LHBF();
	if ( model ) {
	    filename = modelFile.toString();
	    if ( filename.endsWith(".kmr") )
		try { mloob = new LHBF(filename, false); }                                                                         catch ( ClassNotFoundException e ) {}
		catch ( OutOfMemoryError e ) { System.err.println("not enough RAM: " + (modelFile.length()>>20) + "Mb required (java option -Xmx)");   System.exit(1); }
	    if ( filename.endsWith(".kmz") ) 
		try { mloob = new LHBF(filename, true); }                                                                          catch ( ClassNotFoundException e ) {}
		catch ( OutOfMemoryError e ) { System.err.println("not enough RAM: " + (modelFile.length()>>19) + "Mb required (java option -Xmx)");   System.exit(1); }
	    if ( k != mloob.k() ) { System.err.println("the k-mer length of the model file is incorrect: " + mloob.k() + " != " + k + "(option -b)");  System.exit(1); }
	    System.out.println("input model file (k=" + k + "):  " + filename +
	    		       " [b=" + mloob.l2nBits() + ";h=" + mloob.nHash() + ";n=" + String.format(Locale.US, "%,d", mloob.nElements()) + ";fp<" + String.format(Locale.US, "%.4f", mloob.fp()) + "]");
	}

	
	//#############################################################################################################
	//#############################################################################################################
	//### displaying summary                                                                                    ###
	//#############################################################################################################
	//#############################################################################################################
	if ( infq1.toString().equals(NOTHING) ) {
	    chrono(t);
	    System.exit(0);
	}
	if ( ! paired ) System.out.println("FASTQ file:               " + infq1.toString());
	else            System.out.println("FASTQ files:              " + infq1.toString() + "  " + infq2.toString());
	System.out.println("main options:             -k " + k + "  -c " + cutoff );
	if ( ! paired ) {
	    System.out.print("FASTQ outfile:            " + ((gz)?basename+".fastq.gz":basename+".fastq"));
	    if ( wrm ) System.out.println("  " + ((gz)?basename+".rm.fastq.gz":basename+".rm.fastq"));
	    else       System.out.println("");
	}
	else {
	    System.out.print("FASTQ outfiles:           " + basename + ((gz)?".1.fastq.gz":".1.fastq") + "  " + basename + ((gz)?".2.fastq.gz":".2.fastq"));
	    if ( wrm ) System.out.println("  " + ((gz)?basename+".rm.1.fastq.gz":basename+".rm.1.fastq") + "  " + ((gz)?basename+".rm.1.fastq.gz":basename+".rm.2.fastq"));
	    else       System.out.println("");
	}


	//#############################################################################################################
	//#############################################################################################################
	//### filtering out alien reads from FASTQ file(s)                                                          ###
	//#############################################################################################################
	//#############################################################################################################
	if ( basename.equals(NOTHING) ) basename = DEFAULT_BASENAME;
	//## file readers/writers
	filename         = infq1.toString();
	in1              = ( filename.endsWith(".gz") ) ? new BufferedReader(new InputStreamReader(new GZIPInputStream(Files.newInputStream(Path.of(filename)), LARGE_BUFFER_SIZE)))         : Files.newBufferedReader(Path.of(filename));
	if ( ! paired ) {
	    filename     = basename + ".fastq";
	    out1         = ( gz ) ? new BufferedWriter(new OutputStreamWriter(new GZIPOutputStream(Files.newOutputStream(Path.of(filename + ".gz")), LARGE_BUFFER_SIZE)), LARGE_BUFFER_SIZE) : Files.newBufferedWriter(Path.of(filename));
	    if ( wrm ) {
		filename = basename + ".rm.fastq";
		rmout1   = ( gz ) ? new BufferedWriter(new OutputStreamWriter(new GZIPOutputStream(Files.newOutputStream(Path.of(filename + ".gz")), LARGE_BUFFER_SIZE)), LARGE_BUFFER_SIZE) : Files.newBufferedWriter(Path.of(filename));
	    }
	}
	else {	    
	    filename     = infq2.toString();
	    in2          = ( filename.endsWith(".gz") ) ? new BufferedReader(new InputStreamReader(new GZIPInputStream(Files.newInputStream(Path.of(filename)), LARGE_BUFFER_SIZE)))         : Files.newBufferedReader(Path.of(filename));
	    filename     = basename + ".1.fastq";
	    out1         = ( gz ) ? new BufferedWriter(new OutputStreamWriter(new GZIPOutputStream(Files.newOutputStream(Path.of(filename + ".gz")), LARGE_BUFFER_SIZE)), LARGE_BUFFER_SIZE) : Files.newBufferedWriter(Path.of(filename));
	    filename     = basename + ".2.fastq";
	    out2         = ( gz ) ? new BufferedWriter(new OutputStreamWriter(new GZIPOutputStream(Files.newOutputStream(Path.of(filename + ".gz")), LARGE_BUFFER_SIZE)), LARGE_BUFFER_SIZE) : Files.newBufferedWriter(Path.of(filename));
	    if ( wrm ) {
		filename = basename + ".rm.1.fastq";
		rmout1   = ( gz ) ? new BufferedWriter(new OutputStreamWriter(new GZIPOutputStream(Files.newOutputStream(Path.of(filename + ".gz")), LARGE_BUFFER_SIZE)), LARGE_BUFFER_SIZE) : Files.newBufferedWriter(Path.of(filename));
		filename = basename + ".rm.2.fastq";
		rmout2   = ( gz ) ? new BufferedWriter(new OutputStreamWriter(new GZIPOutputStream(Files.newOutputStream(Path.of(filename + ".gz")), LARGE_BUFFER_SIZE)), LARGE_BUFFER_SIZE) : Files.newBufferedWriter(Path.of(filename));
	    }
	}
	
	//## parsing FASTQ file(s)  ##########################################################################
	p = bloom.fp();
	
	while ( (l1_1 = in1.readLine()) != null ) {
	    l1_2      = in1.readLine();    //## NOTE: R1 sequenced read
	    l1_3      = in1.readLine(); 
	    l1_4      = in1.readLine()  ;  //## NOTE: R1 Phred scores

	    lgt       = l1_2.length(); ++cnt_r_in1; cnt_b_in1 += lgt;
	    discard   = ( lgt < k );

	    //## traversing R1  ##############################################################################
	    if ( ! discard ) {
		ba  = l1_2.getBytes();
		kmr = krc = skip = B0;
		j = -1;
		while ( ++j < k_1 ) {
		    kmr <<= BSIZE; krc >>>= BSIZE; skip = ( skip > B0 ) ? --skip : skip;
		    switch ( ba[j] ) { 
		    case UTF8_A: case UTF8_a: /*kmr |= BA;*/ krc |= rcA;   break;
		    case UTF8_C: case UTF8_c:   kmr |= BC;   krc |= rcC;   break;
		    case UTF8_G: case UTF8_g:   kmr |= BG;   krc |= rcG;   break;
		    case UTF8_T: case UTF8_t:   kmr |= BT; /*krc |= rcT;*/ break;
		    default:                    skip = k;                  break; 
		    }
		}
		crit = succ = 0; limit = cutoff * (lgt - k_1 - 2); //## NOTE: discard condition:  crit >= limit;
		--j;
		if ( ! model ) 
		    while ( crit < limit  &&  ++j < lgt ) {
			kmr <<= BSIZE; krc >>>= BSIZE; skip = ( skip > 0 ) ? --skip : skip;
			switch ( ba[j] ) { 
			case UTF8_A: case UTF8_a: /*kmr |= BA;*/ krc |= rcA;   break;
			case UTF8_C: case UTF8_c:   kmr |= BC;   krc |= rcC;   break;
			case UTF8_G: case UTF8_g:   kmr |= BG;   krc |= rcG;   break;
			case UTF8_T: case UTF8_t:   kmr |= BT; /*krc |= rcT;*/ break;
			default:                    skip = k;    succ = 0;     break; 
			}
			if ( skip > B0 )                 { limit -= cutoff;                                         continue; } //## NOTE: skipped k-mer => decreasing normalizing factor (--mu)
			if ( bloom.get(kmr & msk, krc) ) { crit = ( ++succ < WGT_LGT ) ? crit + WGT[succ] : ++crit; continue; } //## NOTE: k-mer match
			if ( succ >= SUCC_MISMATCH  &&                                                                          //## NOTE: alien k-mer with the last nucleotide substituted
			     ( bloom.get((kmr & del)/*| BA*/, (krc & drc)  | rcA)  ||
			       bloom.get((kmr & del)  | BC,   (krc & drc)  | rcC)  ||
			       bloom.get((kmr & del)  | BG,   (krc & drc)  | rcG)  ||
			       bloom.get((kmr & del)  | BT,   (krc & drc)/*| rcT*/) ) ) {
			    crit += k * (1 - p - 1.0 / succ);
			    skip = k;
			    limit += (k_1 * cutoff);
			}
			succ = 0;
		    }
		else 
		    while ( crit < limit  &&  ++j < lgt ) {
			kmr <<= BSIZE; krc >>>= BSIZE; skip = ( skip > 0 ) ? --skip : skip;
			switch ( ba[j] ) { 
			case UTF8_A: case UTF8_a: /*kmr |= BA;*/ krc |= rcA;   break;
			case UTF8_C: case UTF8_c:   kmr |= BC;   krc |= rcC;   break;
			case UTF8_G: case UTF8_g:   kmr |= BG;   krc |= rcG;   break;
			case UTF8_T: case UTF8_t:   kmr |= BT; /*krc |= rcT;*/ break;
			default:                    skip = k;    succ = 0;     break; 
			}
			if ( skip > B0 )                 { limit -= cutoff;                                         continue; } //## NOTE: skipped k-mer => decreasing normalizing factor (--mu)
			if ( mloob.get(kmr & msk, krc) )                                                            continue;   //## NOTE: model k-mer => continue
			if ( bloom.get(kmr & msk, krc) ) { crit = ( ++succ < WGT_LGT ) ? crit + WGT[succ] : ++crit; continue; } //## NOTE: k-mer match
			if ( succ >= SUCC_MISMATCH  &&                                                                          //## NOTE: alien k-mer with the last nucleotide substituted
			     ( bloom.get((kmr & del)/*| BA*/, (krc & drc)  | rcA)  ||
			       bloom.get((kmr & del)  | BC,   (krc & drc)  | rcC)  ||
			       bloom.get((kmr & del)  | BG,   (krc & drc)  | rcG)  ||
			       bloom.get((kmr & del)  | BT,   (krc & drc)/*| rcT*/) ) ) {
			    crit += k * (1 - p - 1.0 / succ);
			    skip = k;
			    limit += (k_1 * cutoff);
			}
			succ = 0;
		    }
		discard = ( crit >= limit );
	    }
	    
	    //## writing single-end outfile(s)  ##############################################################
	    if ( ! paired ) {
		if ( ! discard ) { ++cnt_r_out1; cnt_b_out1 += lgt; writeFQ(  out1, l1_1, l1_2, l1_3, l1_4); continue; }
		if ( wrm )                                          writeFQ(rmout1, l1_1, l1_2, l1_3, l1_4);
		continue;
	    }

	    //## reading R2 FASTQ block  #####################################################################
	    l2_1    = in2.readLine();    
	    l2_2    = in2.readLine();  //## NOTE: R2 sequenced read
	    l2_3    = in2.readLine(); 
	    l2_4    = in2.readLine();  //## NOTE: R2 Phred scores

	    lgt     = l2_2.length(); ++cnt_r_in2; cnt_b_in2 += lgt;

	    //## writing R1 and R2 if (i) wrm is set and (ii) R1 is already discarded  #######################
	    if ( discard ) {
		if ( wrm ) { writeFQ(rmout1, l1_1, l1_2, l1_3, l1_4); writeFQ(rmout2, l2_1, l2_2, l2_3, l2_4); }
		continue;
	    }

	    discard = ( lgt < k );

	    //## traversing R2  ##############################################################################
	    if ( ! discard ) { 
		ba  = l2_2.getBytes();
		kmr = krc = skip = B0;
		j = -1;
		while ( ++j < k_1 ) {
		    kmr <<= BSIZE; krc >>>= BSIZE; skip = ( skip > B0 ) ? --skip : skip;
		    switch ( ba[j] ) { 
		    case UTF8_A: case UTF8_a: /*kmr |= BA;*/ krc |= rcA;   break;
		    case UTF8_C: case UTF8_c:   kmr |= BC;   krc |= rcC;   break;
		    case UTF8_G: case UTF8_g:   kmr |= BG;   krc |= rcG;   break;
		    case UTF8_T: case UTF8_t:   kmr |= BT; /*krc |= rcT;*/ break;
		    default:                    skip = k;                  break; 
		    }
		}
		crit = succ = 0; limit = cutoff * (lgt - k_1 - 2); //## NOTE: discard condition:  crit >= limit;
		--j;
		if ( ! model ) 
		    while ( crit < limit  &&  ++j < lgt ) {
			kmr <<= BSIZE; krc >>>= BSIZE; skip = ( skip > 0 ) ? --skip : skip;
			switch ( ba[j] ) { 
			case UTF8_A: case UTF8_a: /*kmr |= BA;*/ krc |= rcA;   break;
			case UTF8_C: case UTF8_c:   kmr |= BC;   krc |= rcC;   break;
			case UTF8_G: case UTF8_g:   kmr |= BG;   krc |= rcG;   break;
			case UTF8_T: case UTF8_t:   kmr |= BT; /*krc |= rcT;*/ break;
			default:                    skip = k;    succ = 0;     break; 
			}
			if ( skip > B0 )                 { limit -= cutoff;                                         continue; } //## NOTE: skipped k-mer => decreasing normalizing factor (--mu)
			if ( bloom.get(kmr & msk, krc) ) { crit = ( ++succ < WGT_LGT ) ? crit + WGT[succ] : ++crit; continue; } //## NOTE: k-mer match
			if ( succ >= SUCC_MISMATCH  &&                                                                          //## NOTE: alien k-mer with the last nucleotide substituted
			     ( bloom.get((kmr & del)/*| BA*/, (krc & drc)  | rcA)  ||
			       bloom.get((kmr & del)  | BC,   (krc & drc)  | rcC)  ||
			       bloom.get((kmr & del)  | BG,   (krc & drc)  | rcG)  ||
			       bloom.get((kmr & del)  | BT,   (krc & drc)/*| rcT*/) ) ) {
			    crit += k * (1 - p - 1.0 / succ);
			    skip = k;
			    limit += (k_1 * cutoff);
			}
			succ = 0;
		    }
		else 
		    while ( crit < limit  &&  ++j < lgt ) {
			kmr <<= BSIZE; krc >>>= BSIZE; skip = ( skip > 0 ) ? --skip : skip;
			switch ( ba[j] ) { 
			case UTF8_A: case UTF8_a: /*kmr |= BA;*/ krc |= rcA;   break;
			case UTF8_C: case UTF8_c:   kmr |= BC;   krc |= rcC;   break;
			case UTF8_G: case UTF8_g:   kmr |= BG;   krc |= rcG;   break;
			case UTF8_T: case UTF8_t:   kmr |= BT; /*krc |= rcT;*/ break;
			default:                    skip = k;    succ = 0;     break; 
			}
			if ( skip > B0 )                 { limit -= cutoff;                                         continue; } //## NOTE: skipped k-mer => decreasing normalizing factor (--mu)
			if ( mloob.get(kmr & msk, krc) )                                                            continue;   //## NOTE: model k-mer => continue
			if ( bloom.get(kmr & msk, krc) ) { crit = ( ++succ < WGT_LGT ) ? crit + WGT[succ] : ++crit; continue; } //## NOTE: k-mer match
			if ( succ >= SUCC_MISMATCH  &&                                                                          //## NOTE: alien k-mer with the last nucleotide substituted
			     ( bloom.get((kmr & del)/*| BA*/, (krc & drc)  | rcA)  ||
			       bloom.get((kmr & del)  | BC,   (krc & drc)  | rcC)  ||
			       bloom.get((kmr & del)  | BG,   (krc & drc)  | rcG)  ||
			       bloom.get((kmr & del)  | BT,   (krc & drc)/*| rcT*/) ) ) {
			    crit += k * (1 - p - 1.0 / succ);
			    skip = k;
			    limit += (k_1 * cutoff);
			}
			succ = 0;
		    }
		discard = ( crit >= limit );
	    }

	    //## writing R1 and R2 if (i) wrm is set and (ii) R2 is discarded  ###############################
	    if ( discard ) {
		if ( wrm ) { writeFQ(rmout1, l1_1, l1_2, l1_3, l1_4); writeFQ(rmout2, l2_1, l2_2, l2_3, l2_4); }
		continue;
	    }
	    //## writing paired-end outfiles  ################################################################
	    ++cnt_r_out1; cnt_b_out1 += lgt;       ++cnt_r_out2; cnt_b_out2 += lgt;
	    writeFQ(out1, l1_1, l1_2, l1_3, l1_4); writeFQ(out2, l2_1, l2_2, l2_3, l2_4);
	}

	in1.close();
	out1.close();
	if ( wrm ) rmout1.close();
	if ( paired ) {
	    in2.close();
	    out2.close();
	    if ( wrm ) rmout2.close();
	}


	//#############################################################################################################
	//#############################################################################################################
	//### displaying final stats                                                                                ###
	//#############################################################################################################
	//#############################################################################################################
	System.out.println("");
	if ( ! paired ) {
	    x = String.format(Locale.US,"%,d",cnt_r_in1).length(); y = String.format(Locale.US,"%,d",cnt_b_in1).length(); 
	    System.out.println(String.format(Locale.US, "inputted:                 %,"+x+"d reads   %,"+y+"d bases", cnt_r_in1,            cnt_b_in1));
	    System.out.println(String.format(Locale.US, "retained:                 %,"+x+"d reads   %,"+y+"d bases", cnt_r_out1,           cnt_b_out1));
	    System.out.println(String.format(Locale.US, "removed:                  %,"+x+"d reads   %,"+y+"d bases", cnt_r_in1-cnt_r_out1, cnt_b_in1-cnt_b_out1));
	}
	else {
	    x = String.format(Locale.US,"%,d",cnt_r_in1).length(); y = String.format(Locale.US,"%,d",cnt_b_in1).length(); 
	    System.out.println(String.format(Locale.US, "inputted:                 %,"+x+"d R1 reads   %,"+y+"d R1 bases", cnt_r_in1,            cnt_b_in1));
	    System.out.println(String.format(Locale.US, "                          %,"+x+"d R2 reads   %,"+y+"d R2 bases", cnt_r_in2,            cnt_b_in2));
	    System.out.println(String.format(Locale.US, "retained:                 %,"+x+"d R1 reads   %,"+y+"d R1 bases", cnt_r_out1,           cnt_b_out1));
	    System.out.println(String.format(Locale.US, "                          %,"+x+"d R2 reads   %,"+y+"d R2 bases", cnt_r_out2,           cnt_b_out2));
	    System.out.println(String.format(Locale.US, "removed:                  %,"+x+"d R1 reads   %,"+y+"d R1 bases", cnt_r_in1-cnt_r_out1, cnt_b_in1-cnt_b_out1));
	    System.out.println(String.format(Locale.US, "                          %,"+x+"d R2 reads   %,"+y+"d R2 bases", cnt_r_in2-cnt_r_out2, cnt_b_in2-cnt_b_out2));
	}
	chrono(t);
    }

    final static void writeFQ (final BufferedWriter bw, final String line1, final String line2, final String line3, final String line4) throws IOException {
	bw.write(line1); bw.newLine(); bw.write(line2); bw.newLine(); bw.write(line3); bw.newLine(); bw.write(line4); bw.newLine();
    }

    final static void chrono (final long start) {
	long t = (System.currentTimeMillis()-start) / 1000;
	System.out.println("");
	System.out.println("Total running time:       " + (t/60) + "min" + (((t%=60)<10)?" 0":" ") + t + "sec");
	System.out.println("");
    }
    
}



















